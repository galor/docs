---
title: Requirements for deploying a Chromebook in a LAVA lab
weight: 5
---

Before a Chromebook can be deployed in a LAVA lab it has to meet some
requirements:

  - [CCD](../02-ccd) is open.
  - `servod` can setup a Cr50 interface for the Chromebook.
  - The following operations can be issued using at least one debugging
    interface (`SuzyQ`, `Servo v4`), ideally tested OK with both:
    - `dut-control -p <device_port> cold_reset:on`
    - `dut-control -p <device_port> cold_reset:off`
  - The CPU serial console is usable and stable.
  - After powering up the Chromebook, the Depthcharge prompt can be
    reached in the serial console.
  - The Chromebook is able to send/receive packets through an Ethernet
    interface (ideally using the `Servo v4` as a USB-Eth adapter).
  - The Chromebook is able to boot a Linux kernel through TFTP (see
    [Bootloader setup for LAVA](../04-bootloader_setup)
    and the documentation linked there for more info).
