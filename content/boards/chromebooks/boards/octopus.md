---
title: Octopus Chromebooks
---

`octopus` is a board name for x86_64-based Chromebooks. Many vendors make
Chromebooks based on this board, some examples:

  - [Acer Chromebook
  311](http://eu-acerforeducation.acer.com/resources/bett-show-2019-acer-unveils-new-suite-of-11-6-inch-chromebooks/)
  - [Lenovo Chromebook
  S340-14](https://psref.lenovo.com/Product/Lenovo_Laptops/Lenovo_Chromebook_S340)
  - [Samsung Chromebook
  4](https://www.samsung.com/us/computing/chromebooks/under-12/chromebook-4-11-6-32gb-storage-4gb-ram-xe310xba-k01us/)
  - [HP x360 12b-ca0010nr](https://support.hp.com/us-en/document/c06469311)
  - [HP x360 12b-ca0004na](https://support.hp.com/hk-en/document/c06608505)

These chromebooks use 64 bit Intel Gemini Lake processors like the Intel
Celeron N4000 and Celeron N4100. The rest of the specs may vary between
vendors and models.

The Collabora LAVA lab contains the following `octopus` devices:

  - HP x360 12b-ca0010nr
    - [`hp-x360-12b-ca0004na-octopus-cbg-0`](https://lava.collabora.co.uk/scheduler/device/hp-x360-12b-n4000-octopus-cbg-0)
    - [`hp-x360-12b-ca0004na-octopus-cbg-1`](https://lava.collabora.co.uk/scheduler/device/hp-x360-12b-n4000-octopus-cbg-1)
    - [`hp-x360-12b-ca0004na-octopus-cbg-2`](https://lava.collabora.co.uk/scheduler/device/hp-x360-12b-n4000-octopus-cbg-2)


### Debugging interfaces

`octopus` boards have been flashed and tested with both [SuzyQ and Servo
v4](../../01-debugging_interfaces) interfaces.

In the HP Chromebook x360 model 12b-ca0010nr the debug interface is the
left USB-C port.

#### Network connectivity

The Servo v4 interface includes an Ethernet interface with a chipset
supported by Depthcharge (R8152).

#### Known issues

We observed the following issues:

- The R8152 Ethernet driver in Depthcharge doesn't seem reliable when
  working at Gigabit speeds. It's recommeded to configure the link as
  Fast Ethernet when booting over TFTP.
- Depending on the firmware version of the Servo v4, Depthcharge hangs
  while initializing the Ethernet interface. From the Servo v4 FW
  versions we have tested, `servo_v4_v1.1.5690-46ed8a0 2016-12-01
  06:28:35 @build268-m2` is known to cause issues. Version
  `servo_v4_v2.3.22-ecb74cc56 2019-07-23 18:07:15
  @chromeos-legacy-release-us-central2-c-x32-40-2efr` works fine.

### Example kernel command line arguments

```
earlyprintk=uart8250,mmio32,0xfed00000,115200n8 console=ttyS1,115200n8 root=/dev/nfs ip=dhcp rootwait rw nfsroot=192.168.2.100:/srv/nfs/chromebook,v3 nfsrootdebug
```

the IP and path of the NFS share are examples and should be adapted to
the test setup.
